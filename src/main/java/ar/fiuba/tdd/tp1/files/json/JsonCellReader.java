package ar.fiuba.tdd.tp1.files.json;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.actions.factory.ActionFactory;
import ar.fiuba.tdd.tp1.actions.factory.CellActionFactory;
import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.files.ReadException;
import ar.fiuba.tdd.tp1.printers.json.model.JsonCell;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jonathan on 17/10/15.
 */
public class JsonCellReader {

    public static void createCells(String bookName, List<JsonCell> cells, SpreadSheet api) {
        for (Iterator<JsonCell> iterator = cells.iterator(); iterator.hasNext(); ) {
            JsonCell next = iterator.next();
            createSheetIfNecesary(bookName, next, api);
            createCell(bookName, next, api);
        }
    }

    private static void createSheetIfNecesary(String bookName, JsonCell cell, SpreadSheet api) {
        List<String> sheetsInBook = api.getSheetsInBook(bookName);
        if (!sheetsInBook.contains(cell.getSheet())) {
            Action action = ActionFactory.bookAction().createSheet(cell.getSheet());
            api.execute(bookName, action);
        }
    }

    private static void createCell(String bookName, JsonCell cell, SpreadSheet api) {
        String id = cell.getId().toUpperCase();
        Pattern compile = Pattern.compile("^([A-Z]+)([0-9]+)$");
        Matcher matcher = compile.matcher(id);
        if (!matcher.matches()) {
            throw new ReadException("Invalid File Format");
        }
        String col = matcher.group(1);
        String row = matcher.group(2);

        CellActionFactory factory = ActionFactory.cellAction(col, Integer.valueOf(row), cell.getSheet());
        api.execute(bookName, factory.changeFormula(cell.getValue(),api.getRanges(bookName)));
        JsonFormatterReader.readFormatter(bookName, factory, cell.getFormatter(), api);
    }
}
