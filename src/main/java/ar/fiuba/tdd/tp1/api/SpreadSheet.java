package ar.fiuba.tdd.tp1.api;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.queries.Query;

import java.util.*;

/**
 * Api for the client.
 * Created by jonathan on 23/09/15.
 */
public class SpreadSheet {

    private Map<String, Book> books;
    private Stack<Action> undoActionsStack;
    private Stack<Action> redoActionsStack;
    private String activeBookId;
    private Boolean recordingActions;

    public SpreadSheet() {
        books = new HashMap<>();
        undoActionsStack = new Stack<>();
        redoActionsStack = new Stack<>();
        recordingActions = true;
    }

    public void createBook(String name) {
        if (this.books.containsKey(name)) {
            throw new AlreadyExistentBookException();
        }
        books.put(name, new Book(name));
        if (this.books.size() == 1) {
            this.activeBookId = name;
        }
    }


    public void createBookWithDefaultSheet(String name) {
        if (this.books.containsKey(name)) {
            throw new AlreadyExistentBookException();
        }
        Book book = new Book(name);
        book.createSheet("default");
        this.books.put(name, book);
        this.activeBookId = name;
    }

    public void execute(String bookId, Action action) {
        Book book = getCurrentBook(bookId);
        action.setContext(book);
        action.execute();
        if (recordingActions) {
            undoActionsStack.push(action);
            redoActionsStack.clear();
        }
    }

    public void execute(Action action) {
        this.execute(this.getActiveBook(), action);
    }

    public <T> T execute(String bookId, Query<T> query) {
        Book book = getCurrentBook(bookId);
        query.setContext(book);
        return query.execute();
    }

    public <T> T execute(Query<T> query) {
        return this.execute(this.getActiveBook(), query);
    }

    private Book getCurrentBook(String bookId) {
        Book book = this.books.get(bookId);
        if (book == null) {
            throw new InexistentBookException();
        }
        return book;
    }

    public void undo() {
        if (undoActionsStack.size() > 0) {
            Action action = undoActionsStack.pop();
            action.undo();
            redoActionsStack.push(action);
        }
    }

    public void redo() {
        if (redoActionsStack.size() > 0) {
            Action action = redoActionsStack.pop();
            action.execute();
            undoActionsStack.push(action);
        }
    }

    public List<String> getSheetsInBook(String bookId) {
        Book book = getCurrentBook(bookId);
        return book.getAvailableSheets();
    }

    public List<String> getBooks() {
        List<String> list = new ArrayList<>();
        list.addAll(this.books.keySet());
        return list;
    }

    public String getActiveBook() {
        Book book = this.books.get(this.activeBookId);
        if (book == null) {
            throw new InexistentBookException();
        }
        return this.activeBookId;
    }

    public void setActiveBook(String bookId) {
        Book book = this.books.get(bookId);
        if (book == null) {
            throw new InexistentBookException();
        }
        this.activeBookId = bookId;
    }

    public String getActiveSheetId() {
        return this.books.get(this.activeBookId).getActiveSheet().getName();
    }

    public void setActiveSheetId(String sheetId) {
        this.books.get(this.activeBookId).setActiveSheet(sheetId);
    }

    public void startActionRecording() {
        recordingActions = true;
    }

    public void stopActionRecording() {
        recordingActions = false;
    }

    public void closeBook(String bookName) {
        this.books.remove(bookName);
    }
    
    public HashMap<String,String> getRanges(String bookId){
      return getCurrentBook(bookId).getRanges();
    }

    public void createrRangeInCurrentBook(String name, String range) {
      getCurrentBook(getActiveBook()).createRange(name, range);
    }
}
