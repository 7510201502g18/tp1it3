package ar.fiuba.tdd.tp1.api;

/**
 * Exception thrown when there is an illegal action executed
 * Created by jonathan on 26/09/15.
 */
public class IllegalActionException extends RuntimeException {
}
