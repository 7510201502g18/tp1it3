package ar.fiuba.tdd.tp1.api;

/**
 * Exception thrown when there is no book loaded.
 * Created by jonathan on 23/09/15.
 */
public class InexistentBookException extends RuntimeException {

}
