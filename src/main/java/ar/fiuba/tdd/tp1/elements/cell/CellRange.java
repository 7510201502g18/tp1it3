package ar.fiuba.tdd.tp1.elements.cell;

/**
 * Created by jonathan on 12/10/15.
 */
public class CellRange {

    private String startingCol;
    private Integer startingRow;
    private String endingCol;
    private Integer endingRow;

    public CellRange(String startingCol, Integer startingRow, String endingCol, Integer endingRow) {
        setStarting(startingCol, startingRow);
        seEndings(endingCol, endingRow);
    }

    private void seEndings(String endingCol, Integer endingRow) {
        this.endingCol = endingCol;
        this.endingRow = endingRow;
    }

    private void setStarting(String startingCol, Integer startingRow) {
        this.startingCol = startingCol;
        this.startingRow = startingRow;
    }

    public Integer getStartingRow() {
        return startingRow;
    }

    public Integer getEndingRow() {
        return endingRow;
    }

    public String getStartingCol() {
        return startingCol;
    }

    public String getEndingCol() {
        return endingCol;
    }
}
