package ar.fiuba.tdd.tp1.elements.formatter;

/**
 * Created by jonathan on 14/10/15.
 */
public class FormatException extends RuntimeException {

    public FormatException(String message) {
        super(message);
    }
}
