package ar.fiuba.tdd.tp1.elements.sheet;

/**
 * Exception thrown when the cell you are attempting to get is out of range.
 * Created by matias on 10/1/15.
 */
public class InvalidCellRangeException extends RuntimeException {
}
