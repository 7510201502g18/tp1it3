package ar.fiuba.tdd.tp1.elements.cell;

/**
 * Interface for the formulas.
 * Created by matias on 9/19/15.
 */
public interface Formula {

    String evaluate();

    default String toStringAsRootFormula() {
        return "= " + toString();
    }

    ;
}
