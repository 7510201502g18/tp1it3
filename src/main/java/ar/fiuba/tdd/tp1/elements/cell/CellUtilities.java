package ar.fiuba.tdd.tp1.elements.cell;

import ar.fiuba.tdd.tp1.api.IllegalActionException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jonathan on 22/10/15.
 */
public class CellUtilities {

    private static final Pattern CELL_ID_PATTERN = Pattern.compile("^([a-zA-Z]+)([0-9]+)$");

    public static String getCol(String cellid) {
        return getField(1, cellid).toUpperCase();
    }


    private static String getField(Integer field, String cellid) {
        final Matcher matcher = CELL_ID_PATTERN.matcher(cellid);
        if (matcher.matches()) {
            return matcher.group(field);
        } else {
            throw new IllegalActionException();
        }

    }

    public static Integer getRow(String cellId) {
        return Integer.valueOf(getField(2, cellId));
    }
}
