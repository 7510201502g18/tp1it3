package ar.fiuba.tdd.tp1.printers.json;

import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.cell.Formula;
import ar.fiuba.tdd.tp1.elements.printer.Printer;
import ar.fiuba.tdd.tp1.printers.json.model.JsonCell;
import ar.fiuba.tdd.tp1.printers.json.model.JsonFormatter;

import java.util.List;

/**
 * Created by maxi on 12/10/15.
 */
public class JsonCellPrinter implements Printer {

    private Cell cell;
    private String sheet;
    private List<JsonCell> cells;

    public JsonCellPrinter(String sheet, Cell cell, List<JsonCell> output) {
        this.sheet = sheet;
        this.cells = output;
        this.cell = cell;
    }

    @Override
    public void print() {
        JsonFormatter jsonFormatter = new JsonFormatter(cell.getFormatter().getType(), cell.getFormatter().getFormat());
        Formula formula = cell.getFormula();

        JsonCell jsonCell = new JsonCell(sheet, cell.getId(), formula.toStringAsRootFormula(), jsonFormatter);
        this.cells.add(jsonCell);
    }
}
