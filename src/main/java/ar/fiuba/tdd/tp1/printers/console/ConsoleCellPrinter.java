package ar.fiuba.tdd.tp1.printers.console;

import ar.fiuba.tdd.tp1.api.InexistentBookException;
import ar.fiuba.tdd.tp1.elements.book.InexistentSheetException;
import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.cell.CyclicReferenceException;
import ar.fiuba.tdd.tp1.elements.printer.Printer;
import ar.fiuba.tdd.tp1.formulas.InvalidFormulaException;

/**
 * Created by maxi on 12/10/15.
 */
public class ConsoleCellPrinter implements Printer {

    public static final int MAX_LENGTH = 8;
    private Cell cell;

    public ConsoleCellPrinter(Cell cell) {
        this.cell = cell;
    }

    private static String fixedLengthString(String string, int length) {
        return String.format("%1$" + length + "s", string);
    }

    public static void printAsCell(String value) {
        if (value.length() > MAX_LENGTH) {
            StringBuilder strb = new StringBuilder(value.substring(0, MAX_LENGTH - 3));
            strb.append("...");
            value = strb.toString();
        }
        System.out.print(fixedLengthString(value, 8));
    }

    @Override
    public void print() {
        String value;
        try {
            value = cell.getFormattedValue();
        } catch (CyclicReferenceException e) {
            value = "#CYCLREF";
        } catch (InvalidFormulaException e) {
            value = "#BADFORM";
        } catch (InexistentBookException | InexistentSheetException e) {
            value = "#BADREF";
        }

        printAsCell(value);
    }
}
