package ar.fiuba.tdd.tp1.printers.json.model;

import java.util.List;

/**
 * Created by jonathan on 16/10/15.
 */
public class JsonBook {

    private String name;
    private List<JsonCell> cells;


    public JsonBook() {
    }

    public JsonBook(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<JsonCell> getCells() {
        return cells;
    }

    public void setCells(List<JsonCell> cells) {
        this.cells = cells;
    }

    public String getName() {
        return name;
    }
}
