package ar.fiuba.tdd.tp1.queries.factory;

import ar.fiuba.tdd.tp1.elements.printer.Printer;
import ar.fiuba.tdd.tp1.queries.Query;
import ar.fiuba.tdd.tp1.queries.cell.CellFormulaQuery;
import ar.fiuba.tdd.tp1.queries.cell.CellValueQuery;
import ar.fiuba.tdd.tp1.queries.print.ConsolePrinterQuery;
import ar.fiuba.tdd.tp1.queries.print.JsonPrinterQuery;
import ar.fiuba.tdd.tp1.queries.print.SheetCsvPrinteQuery;

/**
 * Creates differents actions that can be executed
 * Created by jonathan on 28/09/15.
 */
public class QueryFactory implements PrinterQueryFactory, CellQueryFactory {

    private String sheetId;
    private String col;
    private Integer row;

    private QueryFactory(String sheetId, String col, Integer row) {
        this.sheetId = sheetId;
        this.col = col;
        this.row = row;
    }

    private QueryFactory() {

    }

    public static PrinterQueryFactory printQuery() {
        return new QueryFactory();
    }

    public static CellQueryFactory cellQuery(String sheetId, String col, Integer row) {
        return new QueryFactory(sheetId, col, row);
    }

    @Override
    public Query<Printer> activeSheetToCSV(String filename) {
        return new SheetCsvPrinteQuery(filename);
    }

    @Override
    public Query<Printer> bookToFile(String filename) {
        return new JsonPrinterQuery(filename);
    }

    @Override
    public Query<Printer> activeSheetToConsole(String startingCol, Integer startingRow) {
        return new ConsolePrinterQuery(startingCol, startingRow);
    }

    @Override
    public Query<String> getFormula() {
        return new CellFormulaQuery(sheetId, col, row);
    }

    @Override
    public Query<String> getValue() {
        return new CellValueQuery(sheetId, col, row);
    }

}
