package ar.fiuba.tdd.tp1.queries.print;

import ar.fiuba.tdd.tp1.elements.printer.Printer;
import ar.fiuba.tdd.tp1.printers.csv.CSVBookPrinter;
import ar.fiuba.tdd.tp1.queries.PrinterQuery;

/**
 * Created by jonathan on 15/10/15.
 */
public class SheetCsvPrinteQuery extends PrinterQuery {

    private String fileName;

    public SheetCsvPrinteQuery(String fileName) {
        this.fileName = fileName;
    }

    @Override
    protected Printer createPrinter() {
        return new CSVBookPrinter(getContext(), fileName);
    }
}
