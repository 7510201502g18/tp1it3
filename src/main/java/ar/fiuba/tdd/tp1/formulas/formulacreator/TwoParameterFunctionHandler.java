package ar.fiuba.tdd.tp1.formulas.formulacreator;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.cell.CellRange;
import ar.fiuba.tdd.tp1.elements.cell.Formula;
import ar.fiuba.tdd.tp1.formulas.InvalidFormula;
import ar.fiuba.tdd.tp1.formulas.RangeFunction;
import ar.fiuba.tdd.tp1.formulas.RangeFunctionFormula;
import ar.fiuba.tdd.tp1.formulas.StringFormula;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by matias on 10/17/15.
 */
public class TwoParameterFunctionHandler extends FunctionHandler {

  public TwoParameterFunctionHandler(Stack<Formula> formulaTree, Book activeBook, HashMap<String, String> ranges) {
    super(formulaTree, activeBook,ranges);
    this.parametersPattern = Pattern.compile("^([a-zA-Z]+)\\(([^\\)]+),([^\\)]+)\\)$");
    setNextLink(null);
    this.allowedFunctions = new HashMap<>();
    this.allowedFunctions.put("CONCAT", RangeFunction.CONCAT);
    this.allowedFunctions.put("LEFT", RangeFunction.LEFT);
    this.allowedFunctions.put("RIGHT", RangeFunction.RIGHT);
  }

  @Override
  public void handle(String string) {
    Matcher matcher = parametersPattern.matcher(string);
    if (matcher.matches()) {
      List<String> params = new ArrayList<>();
      params.add(matcher.group(2));
      params.add(matcher.group(3));
      getFormulaTree().push(createNewFormula(matcher.group(1), params));
    } else {
      this.getNextLink().handle(string);
    }
  }

  private Formula createNewFormula(String name, List<String> parametersList) {
    if (allowedFunctions.containsKey(name)) {
      List<Formula> referenceList = new ArrayList<>();
      for (String current : parametersList) {
        checkParamTypeRange(referenceList, current);
      }
      String parameterString = parametersList.get(0).concat(",").concat(parametersList.get(1));
      return new RangeFunctionFormula(allowedFunctions.get(name), parameterString, referenceList);
    } else {
      return new InvalidFormula("Function ".concat(name).concat(" not recognized"));
    }

  }

  private void checkParamTypeRange(List<Formula> referenceList, String currentParam) {
    if(ranges != null){
      if(ranges.containsKey(currentParam)){
        currentParam = ranges.get(currentParam);
      }
    }
    Matcher sheetRangePatternMatcher = sheetRangePatter.matcher(currentParam);
    if (sheetRangePatternMatcher.matches()) {
      CellRange range = getRange(sheetRangePatternMatcher.group(2));
      referenceList.addAll(getReferenceList(sheetRangePatternMatcher.group(1), range));
    } else {
      checkParamTypeString(referenceList, currentParam);
    }
  }

  private void checkParamTypeString(List<Formula> referenceList, String currentParam) {
    if (currentParam.startsWith("\"") && currentParam.endsWith("\"")) {
      referenceList.add(new StringFormula(currentParam.replaceAll("\"", "")));
    } else {
      checkParamTypeFormula(referenceList, currentParam);
    }
  }

  private void checkParamTypeFormula(List<Formula> referenceList, String currentParam) {
    referenceList.add(FormulaCreator.createFormulaFromInfix("= ".concat(currentParam), this.book, this.book.getRanges()));
  }
}
