package ar.fiuba.tdd.tp1.formulas;

import ar.fiuba.tdd.tp1.elements.cell.Formula;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by matias on 10/16/15.
 */
public class RangeFunctionFormula implements Formula {

    private List<Formula> referenceList;
    private RangeFunction function;
    private String range;

    public RangeFunctionFormula(RangeFunction function, String range, List<Formula> referenceList) {
        this.function = function;
        this.range = range;
        this.referenceList = referenceList;
    }

    @Override
    public String evaluate() {
        List<String> results = new ArrayList<>();
        for (Formula temp : this.referenceList) {
            results.add(temp.evaluate());
        }
        return this.function.apply(results);
    }

    @Override
    public String toString() {
        return this.function.toString().concat("(").concat(range).concat(")");
    }
}
