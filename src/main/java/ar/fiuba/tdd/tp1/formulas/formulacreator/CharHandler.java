package ar.fiuba.tdd.tp1.formulas.formulacreator;

import ar.fiuba.tdd.tp1.elements.cell.Formula;

import java.util.Stack;

/**
 * Abstract link of the chain.
 * Created by matias on 9/26/15.
 */
public abstract class CharHandler {

    private Stack<Formula> formulaTree;
    private CharHandler nextLink;

    public CharHandler(Stack<Formula> formulaTree) {
        this.formulaTree = formulaTree;
        this.nextLink = null;
    }

    public abstract void handle(String string);

    public Stack<Formula> getFormulaTree() {
        return this.formulaTree;
    }

    public CharHandler getNextLink() {
        return this.nextLink;
    }

    public void setNextLink(CharHandler nextLink) {
        this.nextLink = nextLink;
    }

    protected void next(String token) {
        if (this.nextLink != null) {
            this.nextLink.handle(token);
        }
    }
}
