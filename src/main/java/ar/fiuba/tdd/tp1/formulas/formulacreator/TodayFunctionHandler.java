package ar.fiuba.tdd.tp1.formulas.formulacreator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Stack;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.cell.Formula;
import ar.fiuba.tdd.tp1.formulas.StringFormula;

public class TodayFunctionHandler extends CharHandler {

  public TodayFunctionHandler(Stack<Formula> formulaTree, Book activeBook) {
    super(formulaTree);
   }

   public static Boolean isToday(String stringToCheck) {
       return stringToCheck.matches("^TODAY\\(\\)$");
   }

   @Override
   public void handle(String string) {
       if (isToday(string)) {
         DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
         Date date = new Date();
         getFormulaTree().push(new StringFormula(dateFormat.format(date)));
       } else {
         next(string);
       }
   }

}
