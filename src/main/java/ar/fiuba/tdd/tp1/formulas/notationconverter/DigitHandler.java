package ar.fiuba.tdd.tp1.formulas.notationconverter;

import java.util.Deque;

/**
 * Handle the digits
 * Created by jonathan on 26/09/15.
 */
public class DigitHandler extends TokenHandler {


    public DigitHandler(StringBuilder output, Deque<String> stack) {
        super(output, stack);
    }

    @Override
    public void handle(String token) {
        getOutput().append(token).append(' ');
        next(token);
    }

}
