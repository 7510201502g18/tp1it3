package ar.fiuba.tdd.tp1.formulas.formulacreator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Stack;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.cell.Formula;
import ar.fiuba.tdd.tp1.formulas.PrintfFunctionFormula;
import ar.fiuba.tdd.tp1.formulas.ReferenceFormula;
import ar.fiuba.tdd.tp1.formulas.StringFormula;

public class PrintfFunctionHandler extends CharHandler {
  
  private Book activeBook;
  
  public PrintfFunctionHandler(Stack<Formula> formulaTree, Book activeBook) {
    super(formulaTree);
    this.activeBook = activeBook;
  }

  public static Boolean isPrintf(String stringToCheck) {
    return stringToCheck.matches("^Printf.*");
  }

  @Override
  public void handle(String string) {
    if (isPrintf(string)) {
      string = string.replace("Printf(","").replace(")", "");
      String[] parameters = string.split(",");
      List<Formula> referenceList = new ArrayList<>();
      for(int i = 1; i<parameters.length;++i){
        referenceList.add(new ReferenceFormula(parameters[i],activeBook));
      }
      getFormulaTree().push(new PrintfFunctionFormula(parameters[0],referenceList));
    } else {
      next(string);
    }
  }


}
