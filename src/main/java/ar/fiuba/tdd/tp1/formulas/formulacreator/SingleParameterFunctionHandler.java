package ar.fiuba.tdd.tp1.formulas.formulacreator;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.cell.CellRange;
import ar.fiuba.tdd.tp1.elements.cell.Formula;
import ar.fiuba.tdd.tp1.formulas.InvalidFormula;
import ar.fiuba.tdd.tp1.formulas.RangeFunction;
import ar.fiuba.tdd.tp1.formulas.RangeFunctionFormula;

import java.util.HashMap;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by matias on 10/15/15.
 */
public class SingleParameterFunctionHandler extends FunctionHandler {

  public SingleParameterFunctionHandler(Stack<Formula> formulaTree, Book activeBook, HashMap<String, String> ranges) {
    super(formulaTree, activeBook,ranges);
    this.parametersPattern = Pattern.compile("^([a-zA-Z]+)\\(([^\\),]+)\\)$");
    this.allowedFunctions = new HashMap<>();
    this.allowedFunctions.put("AVERAGE", RangeFunction.AVERAGE);
    this.allowedFunctions.put("MAX", RangeFunction.MAX);
    this.allowedFunctions.put("MIN", RangeFunction.MIN);
    setNextLink(null);
  }

  @Override
  public void handle(String string) {
    Matcher matcher = parametersPattern.matcher(string);
    if (matcher.matches()) {
      String name = matcher.group(1);
      String parameter = matcher.group(2);
      if(ranges != null){
        if(ranges.containsKey(parameter)){
          parameter = ranges.get(parameter);
        }
      }
      String[] list = parameter.split("!");
      String sheetId = list[0];
      String range = list[1];
      getFormulaTree().push(getFunction(name, sheetId, parameter, getRange(range)));
    } else {
      this.next(string);
    }

  }

  private Formula getFunction(String string, String sheetId, String rangeId, CellRange range) {
    if (this.allowedFunctions.containsKey(string)) {
      List<Formula> referenceList = getReferenceList(sheetId, range);
      RangeFunctionFormula formula = new RangeFunctionFormula(allowedFunctions.get(string), rangeId, referenceList);
      return formula;
    } else {
      return new InvalidFormula("Funcion ".concat(string).concat(" inexistente."));
    }
  }

}
