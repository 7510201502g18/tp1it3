package ar.fiuba.tdd.tp1.formulas.notationconverter;

import java.util.Deque;

/**
 * Handle the righ parenthesis
 * Created by jonathan on 26/09/15.
 */
public class RightParenthesisHandler extends TokenHandler {

    public RightParenthesisHandler(StringBuilder output, Deque<String> stack) {
        super(output, stack);
        setNext(new DigitHandler(output, stack));
    }

    @Override
    public void handle(String token) {
        if (token.equals(")")) {
            while (!getStack().peek().equals("(")) {
                getOutput().append(getStack().pop()).append(' ');
            }
            getStack().pop();
        } else {
            next(token);
        }

    }

}
