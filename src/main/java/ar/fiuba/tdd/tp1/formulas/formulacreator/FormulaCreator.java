package ar.fiuba.tdd.tp1.formulas.formulacreator;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.cell.Formula;
import ar.fiuba.tdd.tp1.formulas.InvalidFormula;
import ar.fiuba.tdd.tp1.formulas.NumberFormula;
import ar.fiuba.tdd.tp1.formulas.StringFormula;
import ar.fiuba.tdd.tp1.formulas.notationconverter.NotationConverter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

/**
 * Static class that creates formulas from strings.
 * Created by matias on 9/25/15.
 */
public class FormulaCreator {


    public static Formula createFormulaFromInfix(String infix, Book activeBook, HashMap<String, String> ranges) {
        //Check if is complex formula
        if (infix.startsWith("=")) {
            infix = infix.replaceFirst("=", "");
            infix = infix.trim();
            String posfix = NotationConverter.convertToPosfix(infix);
            return createFormulaTree(posfix, activeBook,ranges);
        } else {
            if (NumberHandler.isNumeric(infix)) {
                return new NumberFormula(Float.valueOf(infix));
            } else {
                return new StringFormula(infix);
            }
        }
    }

    private static Formula createFormulaTree(String posfix, Book activeBook, HashMap<String, String> ranges) {
        List<String> list = Arrays.asList(posfix.split(" "));
        Stack<Formula> formulaTree = new Stack<>();
        CharHandler firstHandler = createChainOfResponsibility(formulaTree, activeBook,ranges);
        for (String operand : list) {
            firstHandler.handle(operand);
        }
        if (formulaTree.size() != 1) {
            return new InvalidFormula("Formula not properly formed.");
        }
        return formulaTree.pop();
    }

    public static Formula emptyFormula() {
        return new NumberFormula(0f);
    }


    private static CharHandler createChainOfResponsibility(Stack<Formula> formulaTree, Book activeBook, HashMap<String, String> ranges) {
        CharHandler operationHandler = new OperationHandler(formulaTree);
        operationHandler.setNextLink(new OperationNotSupportedHandler(formulaTree));
        CharHandler referenceHandler = new ReferenceHandler(formulaTree, activeBook);
        referenceHandler.setNextLink(operationHandler);
        CharHandler numberHandler = new NumberHandler(formulaTree);
        numberHandler.setNextLink(referenceHandler);
        CharHandler todayFunctionHandler = new TodayFunctionHandler(formulaTree, activeBook);
        todayFunctionHandler.setNextLink(numberHandler);
        CharHandler singleParameterFunctionHandler = new SingleParameterFunctionHandler(formulaTree, activeBook,ranges);
        singleParameterFunctionHandler.setNextLink(todayFunctionHandler);
        CharHandler twoParamHandler = new TwoParameterFunctionHandler(formulaTree, activeBook,ranges);
        twoParamHandler.setNextLink(singleParameterFunctionHandler);
        CharHandler printfFunctionHandler = new PrintfFunctionHandler(formulaTree, activeBook);
        printfFunctionHandler.setNextLink(twoParamHandler);
        return printfFunctionHandler;
    }
}
