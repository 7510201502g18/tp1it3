package ar.fiuba.tdd.tp1.formulas;

import java.util.ArrayList;
import java.util.List;

import ar.fiuba.tdd.tp1.elements.cell.Formula;

public class PrintfFunctionFormula implements Formula {

  private List<Formula> referenceList;
  private String expression;

  public PrintfFunctionFormula(String expression, List<Formula> referenceList) {
      this.expression = expression;
      this.referenceList = referenceList;
  }

  @Override
  public String evaluate() {
      List<String> results = new ArrayList<>();
      int i = 0;
      String aux = this.expression;
      for (Formula temp : this.referenceList) {
          aux = aux.replace("$"+i," "+temp.evaluate());
          ++i;
      }
      
      return aux;
  }

  @Override
  public String toString() {
      return "Printf".concat("(").concat(expression).concat(")");
  }
}

