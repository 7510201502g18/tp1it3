package ar.fiuba.tdd.tp1.actions.cell;

import java.util.HashMap;

import ar.fiuba.tdd.tp1.actions.CellAction;
import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.cell.Formula;
import ar.fiuba.tdd.tp1.formulas.formulacreator.FormulaCreator;

/**
 * Action executed when modifying a formula.
 * Created by jonathan on 26/09/15.
 */
public class ChangeCellFormulaAction extends CellAction {

    private Formula newFormula;
    private Formula oldFormula;
    private String newFormulaExpression;
    private HashMap<String,String> ranges;
    
    public ChangeCellFormulaAction(String sheetId, String row, Integer column, String newFormula,HashMap<String,String> ranges) {
        super(sheetId, row, column);
        this.newFormulaExpression = newFormula;
        this.ranges = ranges;
    }

    @Override
    public void execute() {
        super.execute();
        newFormula = FormulaCreator.createFormulaFromInfix(newFormulaExpression, getContext(),ranges);
        final Cell cell = this.getCell();
        this.oldFormula = cell.getFormula();
        cell.setFormula(newFormula);
    }

    @Override
    public void undo() {
        super.undo();
        getCell().setFormula(oldFormula);
    }
}
