package ar.fiuba.tdd.tp1.actions.factory;

import java.util.HashMap;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.actions.cell.ChangeCellFormatterAction;
import ar.fiuba.tdd.tp1.actions.cell.ChangeCellFormatterPropertyAction;
import ar.fiuba.tdd.tp1.actions.cell.ChangeCellFormulaAction;
import ar.fiuba.tdd.tp1.actions.sheet.CreateSheetAction;
import ar.fiuba.tdd.tp1.elements.formatter.Formatter;

/**
 * Creates differents actions that can be executed
 * Created by jonathan on 28/09/15.
 */
public class ActionFactory implements BookActionFactory, CellActionFactory {

    private String col;
    private Integer row;
    private String sheetId;

    private ActionFactory() {

    }

    private ActionFactory(String rowId, Integer colId, String sheetId) {
        this.col = rowId;
        this.row = colId;
        this.sheetId = sheetId;
    }

    public static BookActionFactory bookAction() {
        return new ActionFactory();
    }


    public static CellActionFactory cellAction(String col, Integer row, String sheetId) {
        return new ActionFactory(col, row, sheetId);
    }


    @Override
    public Action changeFormula(String formula,HashMap<String,String> ranges) {
        return new ChangeCellFormulaAction(this.sheetId, this.col, this.row, formula,ranges);
    }

    @Override
    public Action changeFormatter(Formatter formatter) {
        return new ChangeCellFormatterAction(this.sheetId, this.col, this.row, formatter);
    }

    @Override
    public Action changeFormatterProperty(String property, String value) {

        return new ChangeCellFormatterPropertyAction(this.sheetId, this.col, this.row, property, value);
    }

    @Override
    public Action createSheet(String id) {
        return new CreateSheetAction(id);
    }
}
