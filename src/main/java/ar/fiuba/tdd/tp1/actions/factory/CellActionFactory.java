package ar.fiuba.tdd.tp1.actions.factory;

import java.util.HashMap;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.elements.formatter.Formatter;

/**
 * Define the methods to be implemented to create cell actions
 * Created by jonathan on 28/09/15.
 */
public interface CellActionFactory {

    Action changeFormula(String formula,HashMap<String,String> ranges);

    Action changeFormatter(Formatter formatter);

    Action changeFormatterProperty(String property, String value);
}
