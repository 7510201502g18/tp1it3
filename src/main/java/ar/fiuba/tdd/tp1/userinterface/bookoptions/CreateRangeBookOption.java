package ar.fiuba.tdd.tp1.userinterface.bookoptions;

import ar.fiuba.tdd.tp1.api.AlreadyExistentBookException;
import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.userinterface.MenuOption;

public class CreateRangeBookOption extends MenuOption {

  private SpreadSheet spreadSheet;

  public CreateRangeBookOption(SpreadSheet spreadSheet) {
      super("Create Range", "Enter new Range name and Range(EA:rango default!A1:A2)", true);
      this.spreadSheet = spreadSheet;
  }


  @Override
  protected String executeSafeErrors(String input) {
    String[] parameters = input.split(" ");
    this.spreadSheet.createrRangeInCurrentBook(parameters[0],parameters[1]);
      return "";
  }

  @Override
  public Boolean validateInput(String input) {
      return input.length() > 0;
  }
}

