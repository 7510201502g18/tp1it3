package ar.fiuba.tdd.tp1.userinterface;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jonathan on 21/10/15.
 */
public class CompositeMenuOption extends MenuOption {

    private static final String INPUT_PREFIX = "Please select an option between 0 - ";
    private static Pattern NUMERIC_PATTERN = Pattern.compile("^[0-9]$");
    protected List<? extends MenuOption> options;
    protected MenuOption selected;

    public CompositeMenuOption(String title, List<? extends MenuOption> options) {
        super(title, "Please select an option between 0 - ", true);
        this.options = options;
    }


    @Override
    public void printTitle() {
        if (selected == null) {
            super.printTitle();
            int idx = 1;
            for (MenuOption opt : options) {
                System.out.println("    " + idx++ + "- " + opt.getTitle());
            }
            System.out.println("    0- Exit");

        } else {
            selected.printTitle();
        }

    }


    @Override
    public MenuOption execute(String input) {

        if (!validateInput(input)) {
            lastError = "Invalid input";
            return this;
        }
        lastError = "";
        Integer indx = Integer.parseInt(input);
        if (indx == 0) {
            return null;
        }

        return executeSelected(input, indx);
    }

    private MenuOption executeSelected(String input, Integer indx) {
        if (selected == null) {

            if (indx <= options.size()) {
                selected = options.get(indx - 1);
                return selected;
            } else {
                lastError = "Wrong Option";
                return this;
            }
        }
        MenuOption prev = selected;
        selected = selected.execute(input);
        prev.reset();
        return selected;
    }

    @Override
    public void printInputMessage() {
        if (selected == null) {
            inputMessage = INPUT_PREFIX + options.size();
            super.printInputMessage();
        } else {
            selected.printInputMessage();
        }
    }

    @Override
    public Boolean validateInput(String input) {
        if (selected == null) {
            final Matcher matcher = NUMERIC_PATTERN.matcher(input);
            return matcher.matches();
        }
        return selected.validateInput(input);
    }

    @Override
    protected String executeSafeErrors(String input) {
        return null;
    }

    @Override
    public void reset() {
        super.reset();
        if (selected != null) {
            selected.reset();
        }
        selected = null;
    }

    @Override
    public Boolean getNeedInput() {
        if (selected == null) {
            return super.getNeedInput();
        }
        return selected.getNeedInput();

    }
}
