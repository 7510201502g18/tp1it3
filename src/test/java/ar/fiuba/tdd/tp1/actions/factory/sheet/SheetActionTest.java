package ar.fiuba.tdd.tp1.actions.factory.sheet;

import ar.fiuba.tdd.tp1.actions.SheetAction;
import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.book.InexistentSheetException;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by jonathan on 3/10/15.
 */
public class SheetActionTest {

    private static String SHEET_1 = "SHEET1";
    private Book book;

    @Before
    public void before() {
        book = new Book();
        book.createSheet(SHEET_1);

    }

    @Test
    public void solveSheet() {
        SheetAction action = new SheetAction(SHEET_1) {
        };
        action.setContext(book);
        action.execute();
    }

    @Test(expected = InexistentSheetException.class)
    public void solveInexistentSheet() {
        SheetAction action = new SheetAction(SHEET_1 + "2") {
            @Override
            public void execute() {
                super.execute();
                this.getSheet();
            }
        };
        action.setContext(book);
        action.execute();
    }


}
