package ar.fiuba.tdd.tp1.printers;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;
import ar.fiuba.tdd.tp1.formulas.formulacreator.FormulaCreator;
import ar.fiuba.tdd.tp1.printers.console.ConsoleBookPrinter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

/**
 * Created by maxi on 12/10/15.
 */
public class ConsolePrinterTest {

    public static final String NEW_SHEET = "New sheet";
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    private Book book;

    @Before
    public void setUpStreams() {
        book = new Book();
        book.createSheet(NEW_SHEET);
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }

    @Test
    public void printWithOneCell() {

        Sheet sheet = book.getSheet(NEW_SHEET);
        sheet.getCell("A", 1).setFormula(FormulaCreator.createFormulaFromInfix("1", book,book.getRanges()));

        ConsoleBookPrinter screenPrinter = new ConsoleBookPrinter(book, "A", 1);
        screenPrinter.print();
        String[] split = outContent.toString().split("\n");
        assertEquals(18, split.length);
        assertEquals("       1||     1.0|     0.0|     0.0|     0.0|     0.0|     0.0|     0.0|     0.0|   "
                + "  0.0|     0.0", split[3]);

    }

    @Test
    public void printWithMultiplesCells() {
        Sheet sheet = book.getSheet(NEW_SHEET);
        sheet.getCell("A", 1).setFormula(FormulaCreator.createFormulaFromInfix("1", book,book.getRanges()));
        sheet.getCell("B", 2).setFormula(FormulaCreator.createFormulaFromInfix("9", book,book.getRanges()));
        ConsoleBookPrinter screenPrinter = new ConsoleBookPrinter(book, "A", 1);
        screenPrinter.print();
        String[] split = outContent.toString().split("\n");
        validateHeader(split, book, sheet);
        assertEquals(18, split.length);
        assertEquals("       1||     1.0|     0.0|     0.0|     0.0|     0.0|     0.0|     0.0|     0.0|   "
                + "  0.0|     0.0", split[3]);
        assertEquals("       2||     0.0|     9.0|     0.0|     0.0|     0.0|     0.0|     0.0|     0.0|   "
                + "  0.0|     0.0", split[4]);

    }

    private void validateHeader(String[] split, Book book, Sheet sheet) {
        assertEquals("Book: " + book.getName(), split[0]);
        assertEquals("Sheet: " + sheet.getName(), split[1]);
    }

    @Test
    public void printWithMultiplesSheets() {
        Book book = new Book();
        book.createSheet(NEW_SHEET);
        Sheet sheet = book.getSheet(NEW_SHEET);
        sheet.getCell("A", 1).setFormula(FormulaCreator.createFormulaFromInfix("1", book,book.getRanges()));
        sheet.getCell("B", 2).setFormula(FormulaCreator.createFormulaFromInfix("9", book,book.getRanges()));
        book.createSheet("Second sheet");
        Sheet otherSheet = book.getSheet("Second sheet");
        otherSheet.getCell("A", 1).setFormula(FormulaCreator.createFormulaFromInfix("1", book,book.getRanges()));
        book.setActiveSheet(NEW_SHEET);
        ConsoleBookPrinter screenPrinter = new ConsoleBookPrinter(book, "B", 2);
        screenPrinter.print();
        String[] split = outContent.toString().split("\n");
        assertEquals(18, split.length);
        assertEquals(" Row/Col||       B|       C|       D|       E|       F|       G|       H|       I|   "
                + "    J|       K", split[2]);
        assertEquals("       2||     9.0|     0.0|     0.0|     0.0|     0.0|     0.0|     0.0|     0.0|   "
                + "  0.0|     0.0", split[3]);
    }

}
