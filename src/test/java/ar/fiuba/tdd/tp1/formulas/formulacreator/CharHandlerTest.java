package ar.fiuba.tdd.tp1.formulas.formulacreator;

import org.junit.Assert;
import org.junit.Test;

/**
 * CharHandler class tests.
 * Created by matias on 10/4/15.
 */
public class CharHandlerTest {

    @Test
    public void createHandlerTest() {
        CharHandler handler = new NumberHandler(null);
        Assert.assertNotNull(handler);
        Assert.assertNull(handler.getFormulaTree());
        Assert.assertNull(handler.getNextLink());
    }

    @Test
    public void setNextLinkHandler() {
        CharHandler handler = new NumberHandler(null);
        CharHandler secondHandler = new NumberHandler(null);
        handler.setNextLink(secondHandler);
        Assert.assertSame(secondHandler, handler.getNextLink());
    }
}
