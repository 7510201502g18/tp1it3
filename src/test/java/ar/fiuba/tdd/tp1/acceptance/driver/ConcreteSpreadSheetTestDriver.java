package ar.fiuba.tdd.tp1.acceptance.driver;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.actions.factory.ActionFactory;
import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.elements.book.InexistentSheetException;
import ar.fiuba.tdd.tp1.elements.cell.CyclicReferenceException;
import ar.fiuba.tdd.tp1.elements.formatter.FormatException;
import ar.fiuba.tdd.tp1.elements.formatter.IdentityFormatter;
import ar.fiuba.tdd.tp1.elements.printer.Printer;
import ar.fiuba.tdd.tp1.files.csv.CsvSheetReader;
import ar.fiuba.tdd.tp1.files.json.JsonBookReader;
import ar.fiuba.tdd.tp1.formatter.CurrencySymbol;
import ar.fiuba.tdd.tp1.formatter.DateFormat;
import ar.fiuba.tdd.tp1.formatter.DateFormatter;
import ar.fiuba.tdd.tp1.formatter.MoneyFormatter;
import ar.fiuba.tdd.tp1.formulas.InvalidFormulaException;
import ar.fiuba.tdd.tp1.queries.Query;
import ar.fiuba.tdd.tp1.queries.factory.QueryFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Concrete implementation of the SpreadSheetTestDriver provided.
 * Created by matias on 10/5/15.
 */
public class ConcreteSpreadSheetTestDriver implements SpreadSheetTestDriver {

    private static Pattern cellIdPatter = Pattern.compile("^([a-zA-Z]+)([\\d]+)$");
    private static String InvalidFormulaString = "Error:BAD_FORMULA";
    private static String workingDir = System.getProperty("user.dir");
    private Map<String, DefaultFormatterCreator> formatters;
    private SpreadSheet spreadSheet;
    private Map<String,String> ranges;

    public ConcreteSpreadSheetTestDriver() {
        this.spreadSheet = new SpreadSheet();
        formatters = new HashMap<>();
        this.ranges = new HashMap<>();
        formatters.put("Date", () -> new DateFormatter(DateFormat.DDMMYYYY));
        formatters.put("Currency", () -> MoneyFormatter.noDecimalsFormatter(CurrencySymbol.AR));
        formatters.put("String", () -> new IdentityFormatter());
    }
    
    public void createRange(String name,String range){
      spreadSheet.createrRangeInCurrentBook(name,range);
    }

    @Override
    public List<String> workBooksNames() {
        return spreadSheet.getBooks();
    }

    @Override
    public void createNewWorkBookNamed(String name) {
        this.spreadSheet.createBookWithDefaultSheet(name);
    }

    @Override
    public void createNewWorkSheetNamed(String workbookName, String name) {
        Action action = ActionFactory.bookAction().createSheet(name);
        this.spreadSheet.execute(workbookName, action);
    }

    @Override
    public List<String> workSheetNamesFor(String workBookName) {
        return this.spreadSheet.getSheetsInBook(workBookName);
    }

    @Override
    public void setCellValue(String workBookName, String workSheetName, String cellId, String value) {
        List<String> list = parseReference(cellId);
        Action action = ActionFactory.cellAction(list.get(0), Integer.parseInt(list.get(1)), workSheetName).changeFormula(value,spreadSheet.getRanges(workBookName));
        spreadSheet.execute(workBookName, action);

    }

    @Override
    public String getCellValueAsString(String workBookName, String workSheetName, String cellId) {
        List<String> list = parseReference(cellId);
        Query<String> query = QueryFactory.cellQuery(workSheetName, list.get(0), Integer.valueOf(list.get(1))).getValue();
        //Query<String> query = new CellValueQuery(workSheetName, list.get(0), Integer.parseInt(list.get(1)));
        try {
            return spreadSheet.execute(workBookName, query);
        } catch (InvalidFormulaException exception) {
            return InvalidFormulaString;
        } catch (InexistentSheetException exception) {
            throw new UndeclaredWorkSheetException();
        } catch (CyclicReferenceException e) {
            throw new BadReferenceException();
        } catch (FormatException e) {
            return "Error:" + e.getMessage();
        }
    }

    @Override
    public double getCellValueAsDouble(String workBookName, String workSheetName, String cellId) {
        try {

            return Double.parseDouble(this.getCellValueAsString(workBookName, workSheetName, cellId));
        } catch (NumberFormatException exception) {
            throw new BadFormulaException();
        }
    }

    @Override
    public void undo() {
        spreadSheet.undo();
    }

    @Override
    public void redo() {
        spreadSheet.redo();
    }

    @Override
    public void setCellFormatter(String workBookName, String workSheetName, String cellId, String formatter, String format) {
        List<String> list = parseReference(cellId);
        Action action = ActionFactory.cellAction(list.get(0), Integer.parseInt(list.get(1)), workSheetName)
                .changeFormatterProperty(formatter, format);
        spreadSheet.execute(workBookName, action);
    }

    @Override
    public void setCellType(String workBookName, String workSheetName, String cellId, String type) {
        List<String> list = parseReference(cellId);
        Action action = ActionFactory.cellAction(list.get(0), Integer.parseInt(list.get(1)), workSheetName)
                .changeFormatter(formatters.get(type).createDefault());
        spreadSheet.execute(workBookName, action);
    }

    @Override
    public void persistWorkBook(String workBookName, String fileName) {
        spreadSheet.setActiveBook(workBookName);
        Query<Printer> persistWorkbook = QueryFactory.printQuery().bookToFile(fileName);
        spreadSheet.execute(persistWorkbook).print();
    }

    @Override
    public void reloadPersistedWorkBook(String fileName) {
        JsonBookReader.openBook(fileName, spreadSheet);
    }

    @Override
    public void saveAsCSV(String workBookName, String sheetName, String path) {
        spreadSheet.setActiveBook(workBookName);
        spreadSheet.setActiveSheetId(sheetName);
        Query<Printer> saveCSV = QueryFactory.printQuery().activeSheetToCSV(path);
        spreadSheet.execute(saveCSV).print();
    }

    @Override
    public void loadFromCSV(String workBookName, String sheetName, String path) {
        path = path.concat(".csv");
        CsvSheetReader reader = new CsvSheetReader(path, workBookName, spreadSheet);
        reader.importToSheet(sheetName);
    }

    @Override
    public int sheetCountFor(String workBookName) {
        return spreadSheet.getSheetsInBook(workBookName).size();
    }


    private List<String> parseReference(String reference) {
        Matcher matcher = cellIdPatter.matcher(reference);
        if (matcher.find()) {
            String row = matcher.group(1);
            String col = matcher.group(2);
            List<String> list = new ArrayList<>();
            list.add(row);
            list.add(col);
            return list;
        } else {
            throw new BadFormulaException();
        }
    }

    @Override
    public String getCellExpression(String workBookName, String workSheetName, String cellId) {
        List<String> list = parseReference(cellId);
        Query<String> query = QueryFactory.cellQuery(workSheetName, list.get(0), Integer.parseInt(list.get(1))).getFormula();
        return spreadSheet.execute(query);
    }
}
