package ar.fiuba.tdd.tp1.files.json;

import ar.fiuba.tdd.tp1.actions.factory.ActionFactory;
import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.elements.formatter.FormatException;
import ar.fiuba.tdd.tp1.formatter.*;
import ar.fiuba.tdd.tp1.queries.factory.QueryFactory;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.text.DecimalFormat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by jonathan on 18/10/15.
 */
public class JsonBookReaderTest {

    private static final String BOOK_1 = "book1";
    private static final String SHEET_1 = "sheet1";
    private static final String SHEET_2 = "sheet2";
    private static final String FORMULA_A1 = "= 2 + 4";
    private static final String FORMULA_B1 = "= 3 + 7";
    private static final String TEST_FILES_PATH = "files/json/";
    private static final String WRONGFILE_1 = TEST_FILES_PATH + "WRONG1.jss";
    private static final String WRONGFILE_2 = TEST_FILES_PATH + "WRONG2.jss";
    private static final char SEPARATOR = '.';
    private SpreadSheet api;

    @AfterClass
    public static void after() {
        File file = new File(BOOK_1 + ".jss");
        file.delete();
    }

    @Before
    public void before() {
        api = new SpreadSheet();
    }

    @Test
    public void saveAndReadCompleteBook() {
        api.createBook(BOOK_1);
        api.execute(BOOK_1, ActionFactory.bookAction().createSheet(SHEET_1));
        api.execute(BOOK_1, ActionFactory.cellAction("A", 1, SHEET_1).changeFormula(FORMULA_A1,api.getRanges(BOOK_1)));
        api.execute(BOOK_1, ActionFactory.cellAction("B", 1, SHEET_1).changeFormatter(new NumberFormatter(3)));
        api.execute(BOOK_1, ActionFactory.cellAction("B", 1, SHEET_1).changeFormula(FORMULA_B1,api.getRanges(BOOK_1)));
        api.execute(BOOK_1, ActionFactory.bookAction().createSheet(SHEET_2));
        api.execute(BOOK_1, ActionFactory.cellAction("A", 2, SHEET_2).changeFormula("09-11-2015",api.getRanges(BOOK_1)));
        api.execute(BOOK_1, ActionFactory.cellAction("A", 2, SHEET_2)
                .changeFormatter(new DateFormatter(DateFormat.MMDDYYYY)));
        api.execute(BOOK_1, ActionFactory.cellAction("B", 1, SHEET_2)
                .changeFormatter(MoneyFormatter.twoDecimalsFormatter(CurrencySymbol.EURO)));
        api.execute(BOOK_1, QueryFactory.printQuery().bookToFile(BOOK_1)).print();

        SpreadSheet api2 = new SpreadSheet();
        JsonBookReader.openBook(BOOK_1 + ".jss", api2);
        assertTrue(api2.getBooks().contains(BOOK_1));
        assertEquals("= 2.0 + 4.0", api2.execute(BOOK_1, QueryFactory.cellQuery(SHEET_1, "A", 1).getFormula()));
        assertEquals("= 3.0 + 7.0", api2.execute(BOOK_1, QueryFactory.cellQuery(SHEET_1, "B", 1).getFormula()));
        assertEquals("11-09-2015", api2.execute(BOOK_1, QueryFactory.cellQuery(SHEET_2, "A", 2).getValue()));
        assertEquals(CurrencySymbol.EURO.getSymbol() + " 0" + SEPARATOR + "00",
                api2.execute(BOOK_1, QueryFactory.cellQuery(SHEET_2, "B", 1).getValue()));
    }

    @Test(expected = FormatException.class)
    public void wrongFormatter() {
        JsonBookReader.openBook(WRONGFILE_1, api);
    }

    @Test(expected = FormatException.class)
    public void wrongFormat() {
        JsonBookReader.openBook(WRONGFILE_2, api);
    }


}
