package ar.fiuba.tdd.tp1.elements;

import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.cell.CellIdIterator;
import ar.fiuba.tdd.tp1.elements.cell.CellRange;
import ar.fiuba.tdd.tp1.elements.sheet.InvalidCellRangeException;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;


public class SheetTest {


    public static final String SHEET1 = "Sheet1";
    private Sheet defaultSheet;

    @Before
    public void before() {
        defaultSheet = new Sheet();
    }

    @Test(expected = IllegalArgumentException.class)
    public void createWithEmptyName() {
        new Sheet("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createWithNullName() {
        new Sheet(null);
    }

    @Test
    public void create() {
        Sheet sheet = new Sheet(SHEET1);
        Assert.assertEquals(SHEET1, sheet.getName());
    }

    @Test
    public void createWithOutName() {
        Assert.assertEquals("New Sheet", defaultSheet.getName());
    }

    @Test
    public void changeName() {
        defaultSheet.changeName(SHEET1);
        Assert.assertEquals(SHEET1, defaultSheet.getName());
    }

    @Test
    public void getNewCell() {
        Cell cell = defaultSheet.getCell("A", 1);
        Assert.assertNotNull(cell);
    }

    @Test
    public void getInexistentCell() {
        Assert.assertFalse("Cell doesnt exist yet", defaultSheet.cellExists("A", 1));
        Cell cell = defaultSheet.getCell("A", 1);
        Assert.assertNotNull(cell);
        Assert.assertTrue("Cell was created", defaultSheet.cellExists("A", 1));
    }

    @Test
    public void getCellTest() {
        Cell newCell = defaultSheet.getCell("A", 1);
        assertSame(newCell, defaultSheet.getCell("A", 1));
        assertSame(newCell, defaultSheet.getCell("a", 1));
        newCell = defaultSheet.getCell("a", 2);
        assertSame(newCell, defaultSheet.getCell("A", 2));
    }

    @Test(expected = InvalidCellRangeException.class)
    public void getCellInvalidRangeTest() {
        defaultSheet.getCell("A", -1);
    }

    @Test
    public void iterateVisitedCells() {
        defaultSheet.getCell("A", 1);
        defaultSheet.getCell("A", 3);
        defaultSheet.getCell("C", 1);
        Iterator<Cell> cellIterator = defaultSheet.visitedCellsIterator();
        int count = 0;
        while (cellIterator.hasNext()) {
            count++;
            cellIterator.next();
        }
        assertEquals(Integer.valueOf(3), Integer.valueOf(count));
    }

    @Test
    public void iterateCellRange() {
        Iterator<Iterator<Cell>> it = defaultSheet.rangeIterator(new CellRange("A", 1, "E", 3));
        CellIdIterator idsIterator = new CellIdIterator(new CellRange("A", 1, "E", 3));
        Integer rowsCount = 0;
        while (it.hasNext()) {
            rowsCount++;
            Iterator<Cell> row = it.next();
            Integer cellsCount = 0;
            while (row.hasNext()) {
                idsIterator.next();
                cellsCount++;
                Cell cell = row.next();
                assertEquals(idsIterator.getCurrentRow(), cell.getRow());
                assertEquals(idsIterator.getCurrentColumn(), cell.getColumn());
            }
            assertEquals(Integer.valueOf(5), cellsCount);
        }
        assertEquals(Integer.valueOf(3), rowsCount);
    }

    @Test
    public void getWorkingCellRange() {
        defaultSheet.getCell("A", 2);
        defaultSheet.getCell("B", 3);
        CellRange workingRange = defaultSheet.getWorkingRange();
        assertEquals("A", workingRange.getStartingCol());
        assertEquals(Integer.valueOf(1), workingRange.getStartingRow());
        assertEquals("B", workingRange.getEndingCol());
        assertEquals(Integer.valueOf(3), workingRange.getEndingRow());
    }

    @Test
    public void getAmpliatedWorkingCellRange() {
        defaultSheet.getCell("A", 2);
        defaultSheet.getCell("B", 3);
        defaultSheet.getCell("AA", 10);
        CellRange workingRange = defaultSheet.getWorkingRange();
        assertEquals("A", workingRange.getStartingCol());
        assertEquals(Integer.valueOf(1), workingRange.getStartingRow());
        assertEquals("AA", workingRange.getEndingCol());
        assertEquals(Integer.valueOf(10), workingRange.getEndingRow());
    }

    @Test
    public void getWorkingRangeIterator() {
        defaultSheet.getCell("A", 2);
        defaultSheet.getCell("B", 3);
        defaultSheet.getCell("AA", 10);
        Iterator<Iterator<Cell>> rows = defaultSheet.workingCellRangeIterator();
        Integer rowsCount = 0;
        while (rows.hasNext()) {
            Iterator<Cell> rowit = rows.next();
            rowsCount++;
            Integer colsCount = 0;
            while (rowit.hasNext()) {
                rowit.next();
                colsCount++;
            }
            assertEquals(Integer.valueOf(27), colsCount);
        }
        assertEquals(Integer.valueOf(10), rowsCount);
    }


}
