package ar.fiuba.tdd.tp1.elements;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.book.InexistentSheetException;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;
import ar.fiuba.tdd.tp1.elements.sheet.SheetReference;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class SheetReferenceTest {

    public static final String SHEET_1 = "Sheet 1";
    private Book book;

    @Before
    public void before() {
        book = new Book();
        book.createSheet(SHEET_1);
    }

    @Test(expected = InexistentSheetException.class)
    public void getInexistentShet() {
        SheetReference reference = new SheetReference("asd", book);
        reference.solve();
    }


    @Test
    public void solveSheet() {
        SheetReference reference = new SheetReference(SHEET_1, book);
        Sheet sheet = reference.solve();
        Assert.assertNotNull(sheet);
    }

    @Test
    public void toStringTest() {
        SheetReference reference = new SheetReference(SHEET_1, book);
        Assert.assertEquals(SHEET_1, reference.toString());
    }

}
