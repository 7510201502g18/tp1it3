package ar.fiuba.tdd.tp1.elements;

import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.cell.Formula;
import ar.fiuba.tdd.tp1.formatter.CurrencySymbol;
import ar.fiuba.tdd.tp1.formatter.MoneyFormatter;
import ar.fiuba.tdd.tp1.formulas.NumberFormula;
import org.junit.Test;

import java.text.DecimalFormat;

import static org.junit.Assert.assertEquals;

/**
 * Cell Tests
 * Created by jonathan on 3/10/15.
 */
public class CellTest {

    @Test
    public void toStringTest() {
        Cell cell = new Cell("", 1);
        NumberFormula formula = new NumberFormula(3f);
        cell.setFormula(formula);
        assertEquals(formula.toString(), cell.toString());
    }

    @Test
    public void newCellTest() {
        Cell cell = new Cell("", 1);
        assertEquals("0.0", cell.getValue());
        assertEquals("0.0", cell.toString());
    }


    @Test
    public void formatedAsMoneyTest() {
        Cell cell = new Cell("", 1);
        Formula formula = new NumberFormula(3f);
        cell.setFormula(formula);
        cell.setFormatter(MoneyFormatter.twoDecimalsFormatter(CurrencySymbol.AR));
        assertEquals(CurrencySymbol.AR.getSymbol() + " 3.00", cell.getFormattedValue());
    }
}
