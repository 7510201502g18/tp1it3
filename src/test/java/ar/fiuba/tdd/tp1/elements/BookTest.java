package ar.fiuba.tdd.tp1.elements;

import ar.fiuba.tdd.tp1.elements.book.AlreadyExistentSheetException;
import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.book.InexistentSheetException;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BookTest {

    @Test
    public void createWithoutName() {
        Book book = new Book();
        assertEquals("default", book.getName());
    }

    @Test
    public void createWithName() {
        String nombre = "test";
        Book book = new Book(nombre);
        assertEquals(nombre, book.getName());
    }

    @Test
    public void modifyName() {
        String nombre = "test";
        Book book = new Book();
        book.setName(nombre);
        assertEquals(nombre, book.getName());
    }

    @Test(expected = AlreadyExistentSheetException.class)
    public void addExistingSheet() {
        Book book = new Book("prueba");
        String nombre = "sheet 1";
        book.createSheet(nombre);
        book.createSheet(nombre);
    }


    @Test(expected = InexistentSheetException.class)
    public void removeInexistentSheet() {
        Book book = new Book("prueba");
        String nombre = "sheet 1";
        book.deleteSheet(nombre);
    }

    @Test
    public void availableSheetsTest() {
        Book book = new Book();
        String firstSheet = "Sheet 1";
        String secondSheet = "Sheet 2";
        book.createSheet(firstSheet);
        Assert.assertTrue(book.getAvailableSheets().contains(firstSheet));
        Assert.assertFalse(book.getAvailableSheets().contains(secondSheet));
        book.createSheet(secondSheet);
        Assert.assertTrue(book.getAvailableSheets().contains(secondSheet));
        Assert.assertTrue(book.getAvailableSheets().contains(firstSheet));
    }

}
