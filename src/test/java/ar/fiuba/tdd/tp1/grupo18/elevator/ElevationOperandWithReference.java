package ar.fiuba.tdd.tp1.grupo18.elevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.actions.factory.ActionFactory;
import ar.fiuba.tdd.tp1.actions.sheet.CreateSheetAction;
import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.queries.Query;
import ar.fiuba.tdd.tp1.queries.cell.CellValueQuery;

public class ElevationOperandWithReference {

  public static final String SHEET_1 = "sheet1";
  private Book book;


  @Before
  public void before() {
    book = new Book();
    Action createSheet = new CreateSheetAction(SHEET_1);
    createSheet.setContext(book);
    createSheet.execute();
  }

  @Test
  public void complexReferenceTestNonCyclic() {
    Action firstAction = ActionFactory.cellAction("A", 1, SHEET_1).changeFormula("= sheet1!A2 ^ sheet1!A3",book.getRanges());
    firstAction.setContext(book);
    Action secondAction = ActionFactory.cellAction("A", 2, SHEET_1).changeFormula("= sheet1!A4 ^ sheet1!A5",book.getRanges());
    secondAction.setContext(book);
    Action thirdAction = ActionFactory.cellAction("A", 3, SHEET_1).changeFormula("= sheet1!A5",book.getRanges());
    thirdAction.setContext(book);
    Action fourthAction = ActionFactory.cellAction("A", 4, SHEET_1).changeFormula("2",book.getRanges());
    fourthAction.setContext(book);
    Action fifthAction = ActionFactory.cellAction("A", 5, SHEET_1).changeFormula("2",book.getRanges());
    fifthAction.setContext(book);
    firstAction.execute();
    secondAction.execute();
    thirdAction.execute();
    fourthAction.execute();
    fifthAction.execute();

    Query<String> consulta = new CellValueQuery(SHEET_1, "A", 1);
    consulta.setContext(book);
    Assert.assertEquals("16.0", consulta.execute());
  }
}
