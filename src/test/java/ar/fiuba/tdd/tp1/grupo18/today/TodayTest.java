package ar.fiuba.tdd.tp1.grupo18.today;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import ar.fiuba.tdd.tp1.acceptance.driver.ConcreteSpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;

public class TodayTest {

  private SpreadSheetTestDriver testDriver;
  
  @Before
  public void setUp() {
      this.testDriver = new ConcreteSpreadSheetTestDriver();
  }

  @Test
  public void sumLiterals() {
      testDriver.createNewWorkBookNamed("tecnicas");
      testDriver.setCellValue("tecnicas", "default", "A1", "= TODAY()");

      DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
      Date date = new Date();
      
      assertEquals(dateFormat.format(date), testDriver.getCellValueAsString("tecnicas", "default", "A1"));
  }
  
}
