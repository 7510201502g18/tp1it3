package ar.fiuba.tdd.tp1.grupo18.elevator;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.actions.sheet.CreateSheetAction;
import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.cell.Formula;
import ar.fiuba.tdd.tp1.formulas.BinaryFormula;
import ar.fiuba.tdd.tp1.formulas.BinaryOperation;
import ar.fiuba.tdd.tp1.formulas.NumberFormula;
import ar.fiuba.tdd.tp1.formulas.ReferenceFormula;

import org.junit.Assert;
import org.junit.Test;


public class ElevationOperandTest {

  @Test
  public void performElevation() {
    Formula firstFormula = new NumberFormula(2.0f);
    Formula secondFormula = new NumberFormula(3.0f);
    Formula elevFormula = new BinaryFormula(firstFormula, secondFormula, BinaryOperation.ELEVATION);
    Assert.assertEquals(Float.parseFloat(elevFormula.evaluate()), 8.0f, 0.0001f);
  }

  @Test
  public void elevationFormulaToString() {
    Formula firstFormula = new NumberFormula(2.0f);
    Formula secondFormula = new NumberFormula(3.0f);
    Formula elevFormula = new BinaryFormula(firstFormula, secondFormula, BinaryOperation.ELEVATION);
    Assert.assertEquals(elevFormula.toString(), "2.0 ^ 3.0");
  }

  @Test
  public void complexFormulaWithReference() {
    Book book = new Book();
    String sheetName = "sheet1";
    Action createSheet = new CreateSheetAction(sheetName);
    createSheet.setContext(book);
    createSheet.execute();
    String referenceString = sheetName + "!A12";
    final ReferenceFormula referenceFormula = new ReferenceFormula(referenceString, book);
    Formula firstFormula = new NumberFormula(5.0f);
    Formula elevationFormula = new BinaryFormula(firstFormula, referenceFormula, BinaryOperation.ELEVATION);
    Assert.assertEquals("5.0", elevationFormula.evaluate());
    Assert.assertEquals("5.0 ^ " + referenceString, elevationFormula.toString());
  }
}
