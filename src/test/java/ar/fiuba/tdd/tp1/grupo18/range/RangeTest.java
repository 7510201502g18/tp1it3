package ar.fiuba.tdd.tp1.grupo18.range;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import ar.fiuba.tdd.tp1.acceptance.driver.ConcreteSpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;

public class RangeTest {
  private ConcreteSpreadSheetTestDriver testDriver;
  
  @Before
  public void setUp() {
      this.testDriver = new ConcreteSpreadSheetTestDriver();
  }

  @Test
  public void concat() {
      testDriver.createNewWorkBookNamed("tecnicas");
      testDriver.setCellValue("tecnicas", "default", "A1", "como");
      testDriver.setCellValue("tecnicas", "default", "A2", "andas");
      testDriver.createRange("rango","default!A1:A2");
      testDriver.setCellValue("tecnicas", "default", "A3", "= CONCAT(rango,default!A1)");
      assertEquals("comoandascomo", testDriver.getCellValueAsString("tecnicas", "default", "A3"));
      testDriver.setCellValue("tecnicas", "default", "A2", "va");
      assertEquals("comovacomo", testDriver.getCellValueAsString("tecnicas", "default", "A3"));
  }
  
  @Test
  public void max() {
      testDriver.createNewWorkBookNamed("tecnicas");
      testDriver.setCellValue("tecnicas", "default", "A1", "= 2");
      testDriver.setCellValue("tecnicas", "default", "A2", "= 3");
      testDriver.createRange("rango","default!A1:A2");
      testDriver.setCellValue("tecnicas", "default", "A3", "= MAX(rango)");
      assertEquals(3, testDriver.getCellValueAsDouble("tecnicas", "default", "A3"),0.001f);
      testDriver.setCellValue("tecnicas", "default", "A2", "= 1");
      assertEquals(2, testDriver.getCellValueAsDouble("tecnicas", "default", "A3"),0.001f);
  }
}
