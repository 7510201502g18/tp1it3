package ar.fiuba.tdd.tp1.grupo18.LeftRight;

import ar.fiuba.tdd.tp1.acceptance.driver.ConcreteSpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.actions.factory.ActionFactory;
import ar.fiuba.tdd.tp1.actions.sheet.CreateSheetAction;
import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.queries.Query;
import ar.fiuba.tdd.tp1.queries.cell.CellValueQuery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Demian on 12/11/2015.
 */
public class LeftRightTest {


    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        this.testDriver = new ConcreteSpreadSheetTestDriver();
    }

    @Test
    public void leftShouldCutOnLeftString() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "Hola mundo");
        testDriver.setCellValue("tecnicas", "default", "A5", "= LEFT(default!A1,4)");
        assertEquals("Hola", testDriver.getCellValueAsString("tecnicas", "default", "A5"));
    }

    @Test
    public void rightShouldCutOnRightString() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "Hola mundo");
        testDriver.setCellValue("tecnicas", "default", "A5", "= RIGHT(default!A1,5)");
        assertEquals("mundo", testDriver.getCellValueAsString("tecnicas", "default", "A5"));
    }

    @Test
    public void leftShouldCutOnLeftStringInFormula() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A5", "= LEFT(\"Mundo\u00A0loco\",5)");
        assertEquals("Mundo", testDriver.getCellValueAsString("tecnicas", "default", "A5"));
    }

    @Test
    public void rightShouldCutOnRightStringInFormula() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A5", "= RIGHT(\"Hola\u00A0mundo\",5)");
        assertEquals("mundo", testDriver.getCellValueAsString("tecnicas", "default", "A5"));
    }
}
