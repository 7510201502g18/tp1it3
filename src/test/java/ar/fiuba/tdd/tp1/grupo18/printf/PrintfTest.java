package ar.fiuba.tdd.tp1.grupo18.printf;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import ar.fiuba.tdd.tp1.acceptance.driver.ConcreteSpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;

public class PrintfTest {

  private SpreadSheetTestDriver testDriver;
  
  @Before
  public void setUp() {
      this.testDriver = new ConcreteSpreadSheetTestDriver();
  }

  @Test
  public void printf() {
      testDriver.createNewWorkBookNamed("tecnicas");
      testDriver.setCellValue("tecnicas", "default", "A1", "como");
      testDriver.setCellValue("tecnicas", "default", "A2", "andas");
      testDriver.setCellValue("tecnicas", "default", "A3", "= Printf(Hola$0$1,default!A1,default!A2)");
      assertEquals("Hola como andas", testDriver.getCellValueAsString("tecnicas", "default", "A3"));
      testDriver.setCellValue("tecnicas", "default", "A2", "va");
      assertEquals("Hola como va", testDriver.getCellValueAsString("tecnicas", "default", "A3"));
  }
  
}
